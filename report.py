# -*- coding: utf-8 -*-
from trytond.report import Report

__all__ = ['ReportHistory']

class ReportHistory(Report):
    __name__ = 'gnuhealth.report.evaluation.history'

